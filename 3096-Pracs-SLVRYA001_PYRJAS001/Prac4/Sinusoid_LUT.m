Fs = 150;                   % sampling frequency
dt = 1/Fs;                   % seconds per sample
t = (0:dt:1);                % generate time array
   
%Sine wave 
y = (1023/2)*sin(2*pi*t)+(1023/2);        % generate sin wave array
sine_LUT = round(y);
fprintf('%s\n', join(string(sine_LUT), ','));
figure;
plot(t,y);
title('Sine wave');
