Fs = 150;                   % sampling frequency
dt = 1/Fs;                  % seconds per sample
t = (0:dt:1);               % generate time array

%Triangle wave variables
y = (1023/2)*sawtooth(2*pi*t,0.5)+(1023/2);
triangle_LUT = round(y);
fprintf('%s\n', join(string(triangle_LUT), ','));
figure;
plot(t,y);
title('Triangle wave');
