Fs = 150;                    % sampling frequency
dt = 1/Fs;                   % seconds per sample
t = (0:dt:1);                % generate time array

%Sawtooth wave
y = (1023/2)*sawtooth(2*pi*t)+(1023/2);        % generate sin wave array
sawtooth_LUT = round(y);
fprintf('%s\n', join(string(sawtooth_LUT), ','));
figure;
plot(t,y);
title('Sawtooth wave');

